************************************************************
การเรียนรู้ของเครื่องจากข้อความ (30 นาที)
************************************************************

.. youtube:: D2ARUq0iX5Y
   :width: 640
   :height: 480

.. raw:: html

   <iframe src="https://www.slideshare.net/slideshow/embed_code/key/5MwxlnbOPspIaa" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/CSUBU/47machinelearningwithtextdatapdf" title="4.7-Machine-Learning-with-Text-Data.pdf" target="_blank">4.7-Machine-Learning-with-Text-Data.pdf</a> </strong> from <strong><a href="https://www.slideshare.net/CSUBU" target="_blank">CSUBU</a></strong> </div>
