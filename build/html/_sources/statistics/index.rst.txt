============================================================
สถิติ (3 ชม.)
============================================================

.. toctree::
   :maxdepth: 1
   :caption: หัวข้อย่อย:

   3.1-installation
   3.2-data-input
   3.3-visualization
   3.4-dist-hypo
   3.5-test
   3.6-stats-model


