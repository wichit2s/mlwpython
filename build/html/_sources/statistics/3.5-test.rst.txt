************************************************************
การทดสอบค่าเฉลี่ยของข้อมูลเชิงตัวเลข (30 นาที)
************************************************************

การกระจายของค่าเฉลี่ย
============================================================

One sample t-Test
------------------------------------------------------------

.. code:: python

   t, pVal = stats.ttest_1samp(data, checkValue)


Wilcoxon Signed Rank Sum Test
------------------------------------------------------------

.. code:: python

   (rank, pVal) = stats.wilcoxon(data - checkValue)

เปรียบเทียบระหว่างกลุ่ม
============================================================


Paired t-Test
------------------------------------------------------------

.. code:: python

   import numpy as np
   from scipy import stats

   np.random.seed(1)
   data = np.random.randn(10) + 0.1
   data1 = np.random.randn(10*5
   data2 = data1 + data

   print(stats.ttest_1samp(data, 0)

   print(stats.ttest_rel(data2, data1)

t-Test Independent Groups (unpaired)
------------------------------------------------------------

.. code:: python

  t_stat, pVal = stats.ttest_ind(group1, group2)

Mann-Whitney Test (non-parametric)
------------------------------------------------------------

.. code:: python

   u_stat, pVal = stats.mannwhitneyu(group1, group2)

classical statistical hypothesis test
------------------------------------------------------------

.. code:: python

  import numpy as np
  from scipy import stats
  np.random.seed(1)
  d1 = np.round(np.random.randn(20)*10+90) 
  d2 = np.round(np.random.randn(20)*10+85)
  (t, pVal) = stats.ttest_rel (d1, d2)
  print(f'The probability that the two distributions are equal is {pVal:5.3f}.')


modeling statistical hypothesis test
------------------------------------------------------------

.. code:: python

  import pandas as pd
  import statsmodels.formula.api as sm
  np.random.seed(1)
  d1 = np.round(np.random.randn(20)*10+90) 
  d2 = np.round(np.random.randn(20)*10+85)

  df = pd.DataFrame({'D1': d1, 'D2':d2})
  result = sm.ols(formula='I(d2-d1) ~ 1', data=df).fit()
  print(result.summary())


เปรียบเทียบหลายกลุ่ม 
============================================================

ANOVA
------------------------------------------------------------

* oneway `scipy`


.. code:: python

   from scipy import stats

   f, pVal = stats.f_oneway(d1, d2, d3)

* oneway `statsmodels`

.. code:: python

  import pandas as pd
  from statsmodels.formula.api import ols 
  from statsmodels.stats.anova import anova_lm

  df = pd.DataFrame(data, columns=['value', 'treatment']) 
  model = ols('value ~ C(treatment)', df).fit() 
  anovaResults = anova_lm(model)
  print(anovaResults)

Bonferroni Correction
------------------------------------------------------------

.. code:: python

   from statsmodels.sandbox.stats.multicom import multipletests

   multipletests([.05, .3, .01], method='bonferroni')

Two-way ANOVA
============================================================

.. code:: python

  import pandas as pd
  from statsmodels.formula.api import ols 
  from statsmodels.stats.anova import anova_lm
  
  data = pd.read_xlsx('data.xlsx')
 
  df = pd.DataFrame(data, columns=['hs', 'fetus', 'observer'])

  formula = 'hs ~ C(fetus) + C(observer) + C(fetus):C(observer)' 
  lm = ols(formula, df).fit()
  anovaResults = anova_lm(lm)

  print(anovaResults)

three-way ANOVA
============================================================

.. code:: python

  import matplotlib.pyplot as plt 
  import seaborn as sns 
  sns.set(style="whitegrid")

  df = sns.load_dataset("exercise")
  sns.factorplot("time", "pulse", 
        hue="kind", col="diet", 
        data=df, hue_order=["rest", "walking", "running"],
        palette="YlGnBu_d", aspect=.75
      ).despine(left=True)

  plt.show()
