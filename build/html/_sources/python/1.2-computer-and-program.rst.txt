****************************************
คอมพิวเตอร์และโปรแกรม (15 นาที)
****************************************

Computer Hardware
========================================

.. figure:: images/hardware.png
   :alt: hardware

   hardware

   
Computer Software (Programs)
========================================

A. [แปลง] Compiled Programming Languages
----------------------------------------

.. figure:: images/compiled-program.png
   :alt: compile-program

   compile-program

ตัวอย่างภาษาโปรแกรมที่เป็นแบบ Compiled
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `C <https://en.cppreference.com/w/c>`__
-  `C++ <https://en.cppreference.com/w/>`__
-  `Go <https://go.dev/>`__
-  `Java <https://www.java.com/en/>`__
-  `Swift <https://www.swift.org/>`__
-  `Rust <https://www.rust-lang.org/>`__

B. [แปล] Interpreted Program
----------------------------------------

.. figure:: images/interpreted-program.png
   :alt: interpreted-program

   interpreted-program

ตัวอย่างภาษาโปรแกรมที่เป็น interpreted
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `Python <https://www.python.org/>`__
-  `TypeScript <https://www.typescriptlang.org/>`__
-  `JavaScript <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide>`__
-  `PHP <https://www.php.net/manual/en/langref.php>`__
-  `R <https://cran.r-project.org/doc/manuals/r-release/R-intro.html>`__
-  `Lisp <https://quickdocs.org/>`__


ไพธอน (Python)
========================================

-  ภาษาโปรแกรมเชิงวัตถุสำหรับใช้งานหลายด้าน
-  รองรับหลายระบบปฏิบัติการ
-  เขียนง่าย อ่านง่าย
-  มีชุดคำสั่งเสริมมากมาย สำหรับหลากหลายด้าน

ทำไมไพธอนมีคนใช้เยอะ?
============================================================

เป็นภาษาโปรแกรมที่นิยมใช้ในการพัฒนาซอฟตแวร์
------------------------------------------------------------

-  `Top 10 Most Popular Programming Languages to Learn in
   2020 <https://www.northeastern.edu/graduate/blog/most-popular-programming-languages/>`__
   ``Northe Eastern University``
-  `10 Most Popular Programming Languages in October 2020: Learn To
   Code <https://www.guru99.com/best-programming-language.html>`__
   ``guru99``
-  `TIOBE Index <https://www.tiobe.com/tiobe-index/>`__

เป็นภาษาโปรแกรมที่นิยมใช้ในงานด้านวิทยาการข้อมูล
------------------------------------------------------------

-  `Top programming languages use to compete in kaggle
   competition <https://www.kaggle.com/tvirot/top-languages-used>`__
   ``kaggle``
-  `Top 10 Data Science Programming Languages for
   2020 <https://www.analyticsinsight.net/top-10-data-science-programming-languages-for-2020/>`__
   ``analyticsinsight``
-  `Top Programming Languages for Data Science in
   2020 <https://www.geeksforgeeks.org/top-programming-languages-for-data-science-in-2020/>`__
   ``geeksforgeeks``
-  `Top Data Science Programming
   Languages <https://jelvix.com/blog/top-data-science-programming-languages>`__
   ``jelvix``
-  `Top 7 Best Programming Languages for Data
   Science <https://techbiason.com/programming-languages-for-data-science/>`__
   ``techbiason``

เป็นภาษาโปรแกรมที่ได้รับความนิยมในการพัฒนาระบบ IoT
------------------------------------------------------------

   -  `Top Programming Languages for IoT
      Projects <https://orangesoft.co/blog/top-programming-languages-for-iot-projects>`__
      ``orangesoft``
   -  `7 Best Languages to Learn IoT Development in
      2020 <https://www.geeksforgeeks.org/7-best-languages-to-learn-iot-development-in-2020/>`__
      ``geeksforgeeks``
   -  `Top 8 Programming Languages for IoT (with
      Examples) <https://howtocreateapps.com/programming-languages-iot/>`__
      ``howtocreateapps``
   -  `What programming languages rule the Internet of
      Things? <https://www.networkworld.com/article/3336867/what-programming-languages-rule-the-internet-of-things.html>`__
      ``networkworld``
   -  `Best Programming Languages for IoT in
      2020 <https://www.electronicsmedia.info/2020/11/19/best-programming-languages-for-iot-in-2020/>`__
      ``electronicsmedia``


